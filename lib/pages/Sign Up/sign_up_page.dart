import 'package:dsf/pages/Sign%20Up/sign_up_form.dart';
import 'package:flutter/material.dart';

class SignUpPage extends StatefulWidget {
  @override
  _SignUpPageState createState() => _SignUpPageState();
}

class _SignUpPageState extends State<SignUpPage> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.black,
      body: WillPopScope(
        onWillPop: () {
          Navigator.pop(context);
        },
        child: Stack(
          children: [
            SignupForm(),
          ],
        ),
      ),
    );
  }
}
