import 'dart:io';
import 'dart:ui';

import 'package:datetime_picker_formfield/datetime_picker_formfield.dart';
import 'package:dropdown_search/dropdown_search.dart';
import 'package:dsf/model/member_branch_model.dart';
import 'package:dsf/services/services.dart';
import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_icons/flutter_icons.dart';
import 'package:google_fonts/google_fonts.dart';
import 'dart:async';
import 'package:http/http.dart' as http;
import 'dart:convert';
import 'package:intl/intl.dart';

import 'package:image_picker/image_picker.dart';
import 'package:searchable_dropdown/searchable_dropdown.dart';

class SignupForm extends StatefulWidget {
  @override
  _SignupFormState createState() => _SignupFormState();
}

class _SignupFormState extends State<SignupForm> {
  final GlobalKey<FormState> _formKey = GlobalKey<FormState>();
  TextEditingController _usernameController = TextEditingController();
  TextEditingController _passwordController = TextEditingController();
  TextEditingController _passwordVerifyController = TextEditingController();
  TextEditingController _phoneController = TextEditingController();
  TextEditingController _emailController = TextEditingController();
  TextEditingController _idCardController = TextEditingController();
  TextEditingController _addressController = TextEditingController();
  TextEditingController _dateOfBirthController = TextEditingController();
  DateTime _dateTime;
  String branchName;
  String branch_id;

  final format = DateFormat("yyyy-MM-dd");
  List memberBranch;
  List<MemberBranch> MemberBranch_;

  bool _isObscure = true;
  bool _isLoading = false;

  @override
  void initState() {
    MemberBranchServices.getMemberBranch().then((memberServices) {
      setState(() {
        MemberBranch_ = memberServices;
      });
    });
    _memberBranch();
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Form(
      key: _formKey,
      child: SafeArea(
        child: Center(
          child: SingleChildScrollView(
            padding: EdgeInsets.symmetric(horizontal: 32.0, vertical: 16.0),
            physics: BouncingScrollPhysics(),
            child: Column(
              mainAxisAlignment: MainAxisAlignment.center,
              mainAxisSize: MainAxisSize.max,
              children: [
                Row(
                  mainAxisAlignment: MainAxisAlignment.start,
                  children: [
                    Container(
                      margin: EdgeInsets.only(top: 20, left: 24),
                      padding: EdgeInsets.all(1),
                      child: GestureDetector(
                        onTap: () {
                          Navigator.pop(context);
                        },
                        child: Icon(
                          FontAwesome.long_arrow_left,
                          color: Colors.white,
                          size: 30,
                        ),
                      ),
                    ),
                  ],
                ),
                //showImage(),
                SizedBox(height: 8.0),
                Row(
                  crossAxisAlignment: CrossAxisAlignment.center,
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: <Widget>[
                    Text(
                      'Duo',
                      style: GoogleFonts.rubik(
                        fontWeight: FontWeight.w800,
                        fontSize: 32.0,
                        color: Colors.white,
                      ),
                    ),
                    SizedBox(
                      width: 5,
                    ),
                    Text(
                      'Shine Fitness',
                      style: GoogleFonts.rubik(
                          fontWeight: FontWeight.w400,
                          fontSize: 32.0,
                          color: Color(0xFFf5c518)),
                    ),
                  ],
                ),

                SizedBox(height: 10),
                Text(
                  'Welome! Create your new account.',
                  style: TextStyle(
                      color: Colors.white, fontWeight: FontWeight.w600),
                ),
                SizedBox(height: 56.0),

                Card(
                  elevation: 0.0,
                  color: Colors.blueGrey[50],
                  child: TextFormField(
                    controller: _usernameController,
                    decoration: InputDecoration(
                      border: InputBorder.none,
                      labelText: 'Name',
                      prefixIcon: Icon(Icons.person),
                    ),
                    validator: (value) {
                      if (value.isEmpty) return 'Required';
                    },
                  ),
                ),

                Card(
                  elevation: 0.0,
                  color: Colors.blueGrey[50],
                  child: TextFormField(
                    controller: _phoneController,
                    decoration: InputDecoration(
                      border: InputBorder.none,
                      labelText: 'Phone',
                      prefixIcon: Icon(Icons.phone),
                    ),
                    validator: (value) {
                      if (value.isEmpty) return 'Required';
                    },
                  ),
                ),

                Card(
                  elevation: 0.0,
                  color: Colors.blueGrey[50],
                  child: TextFormField(
                    controller: _emailController,
                    decoration: InputDecoration(
                      border: InputBorder.none,
                      labelText: 'Email',
                      prefixIcon: Icon(Icons.email),
                    ),
                    validator: (value) {
                      if (value.isEmpty) return 'Required';
                    },
                  ),
                ),

                Card(
                  elevation: 0.0,
                  color: Colors.blueGrey[50],
                  child: TextFormField(
                    controller: _idCardController,
                    decoration: InputDecoration(
                      border: InputBorder.none,
                      labelText: 'ID CARD/KTP',
                      prefixIcon: Icon(Icons.card_membership),
                    ),
                    validator: (value) {
                      if (value.isEmpty) return 'Required';
                    },
                  ),
                ),

                Card(
                  elevation: 0.0,
                  color: Colors.blueGrey[50],
                  child: TextFormField(
                    controller: _addressController,
                    decoration: InputDecoration(
                      border: InputBorder.none,
                      labelText: 'Address',
                      prefixIcon: Icon(Icons.home),
                    ),
                    validator: (value) {
                      if (value.isEmpty) return 'Required';
                    },
                  ),
                ),

                Card(
                  elevation: 0.0,
                  color: Colors.blueGrey[50],
                  child: DateTimeField(
                    format: format,
                    controller: _dateOfBirthController,
                    decoration: InputDecoration(
                      labelText: 'Date Of Birth',
                      prefixIcon: Icon(Icons.calendar_today),
                    ),
                    onShowPicker: (context, currentValue) async {
                      final result = await showDatePicker(
                        context: context,
                        firstDate: DateTime(1900),
                        initialDate: currentValue ?? DateTime.now(),
                        lastDate: DateTime(2100),
                      );

                      return result;
                    },
                  ),
                ),

                //======================================>Member Branch
                Card(
                  elevation: 0.0,
                  color: Colors.blueGrey[50],
                  child: (memberBranch!=null)
                      ?
                  DropdownSearch(
                    dropdownSearchDecoration: InputDecoration(
                      border: InputBorder.none,
                      prefixIcon: Icon(Icons.show_chart),
                      contentPadding: EdgeInsets.all(0),
                    ),
                    mode: Mode.BOTTOM_SHEET,
                    label: 'Member Branch',
                    items: memberBranch.map((e) => e['branch_name']).toList(),
                    showSearchBox: true,
                    selectedItem: branchName,
                    onChanged: (value) {
                      setState(() {
                        branchName = value;
                      });

                      final id = memberBranch.firstWhere(
                              (element) => element['branch_name'] == branchName);

                      setState(() {
                        branch_id = id['branch_id'].toString();
                      });

                      print(branchName);
                      print(branch_id);
                    },
                  ):SizedBox(),
                ),

                Card(
                  elevation: 0.0,
                  color: Colors.blueGrey[50],
                  child: TextFormField(
                    controller: _passwordController,
                    decoration: InputDecoration(
                      border: InputBorder.none,
                      labelText: 'Password',
                      prefixIcon: Icon(Icons.vpn_key),
                    ),
                    validator: (val){
                      if(val.isEmpty)
                        return 'Empty';
                      return null;
                    },
                    obscureText: true,
                  ),
                ),

                Card(
                  elevation: 0.0,
                  color: Colors.blueGrey[50],
                  child: TextFormField(
                    controller: _passwordVerifyController,
                    decoration: InputDecoration(
                      border: InputBorder.none,
                      labelText: 'Verify Password',
                      prefixIcon: Icon(Icons.vpn_key),
                    ),
                    validator: (val){
                      if(val.isEmpty)
                        return 'Empty';
                      if(val != _passwordController.text)
                        return 'Not Match';
                      return null;
                    },
                    obscureText: true,
                  ),
                ),
                // MaterialButton(
                //     color: Colors.yellow,
                //     child: Text('check value'),
                //     onPressed: () {
                //       pickImageFromGallery(ImageSource.gallery);
                //     }),

                SizedBox(
                  height: 30,
                ),

                Row(
                  mainAxisAlignment: MainAxisAlignment.spaceAround,
                  children: [
                    Text("KTP IMAGE",
                      style: TextStyle(
                        color: Colors.white
                      ),
                    ),
                    Text("USER IMAGE",
                      style: TextStyle(
                        color: Colors.white
                      ),
                    ),
                  ],
                ),

                SizedBox(
                  height: 20,
                ),

                Row(
                  mainAxisAlignment: MainAxisAlignment.spaceAround,
                  children: [
                    Center(
                      child: GestureDetector(
                        onTap: () {
                          _showPickerKTP(context);
                        },
                        child: CircleAvatar(
                          radius: 55,
                          backgroundColor: Color(0xffFDCF09),
                          child: ktpImage != null
                              ? ClipRRect(
                                  borderRadius: BorderRadius.circular(50),
                                  child: Image.file(
                                    ktpImage,
                                    width: 100,
                                    height: 100,
                                    fit: BoxFit.fitHeight,
                                  ),
                                )
                              : Container(
                                  decoration: BoxDecoration(
                                      color: Colors.grey[200],
                                      borderRadius: BorderRadius.circular(50)),
                                  width: 100,
                                  height: 100,
                                  child: Icon(
                                    Icons.credit_card,
                                    color: Colors.grey[800],
                                  ),
                                ),
                        ),
                      ),
                    ),
                    Center(
                      child: GestureDetector(
                        onTap: () {
                          _showPickerUser(context);
                        },
                        child: CircleAvatar(
                          radius: 55,
                          backgroundColor: Color(0xffffff00),
                          child: imageFile != null
                              ? ClipRRect(
                                  borderRadius: BorderRadius.circular(50),
                                  child: Image.file(
                                    imageFile,
                                    width: 100,
                                    height: 100,
                                    fit: BoxFit.fitHeight,
                                  ),
                                )
                              : Container(
                                  decoration: BoxDecoration(
                                      color: Colors.grey[200],
                                      borderRadius: BorderRadius.circular(50)),
                                  width: 100,
                                  height: 100,
                                  child: Icon(
                                    Icons.person,
                                    color: Colors.grey[800],
                                  ),
                                ),
                        ),
                      ),
                    ),
                  ],
                ),

                SizedBox(
                  height: 20,
                ),

                Container(
                  width: MediaQuery.of(context).size.width,
                  child: RaisedButton(
                    color: Color(0xFFf5c518),
                    child: (_isLoading == false)
                        ? Text(
                            "Register",
                            style: TextStyle(color: Colors.black),
                          )
                        : CircularProgressIndicator(
                            backgroundColor: Colors.blueAccent,
                          ),
                    onPressed: () {
                      if (_formKey.currentState.validate()) {
                        setState(() {
                          _isLoading = true;
                        });
                        _registerUser();
                      }
                    },
                  ),
                ),
              ],
            ),
          ),
        ),
      ),
    );
  }

  Future<List> member_branch() async {
    final response = await http.get(
      'http://10.0.2.2:8181/branch/member_branch',
    );

    var result = json.decode(response.body);

    print(result);
  }

  //NOTE:Option chooser
  void _showPickerKTP(context) {
    showModalBottomSheet(
        context: context,
        builder: (BuildContext bc) {
          return SafeArea(
            child: Container(
              child: new Wrap(
                children: <Widget>[
                  new ListTile(
                      leading: new Icon(Icons.photo_library),
                      title: new Text('Photo Library'),
                      onTap: () {
                        _ktpFromGallery();
                        Navigator.of(context).pop();
                      }),
                  new ListTile(
                    leading: new Icon(Icons.photo_camera),
                    title: new Text('Camera'),
                    onTap: () {
                      _ktpFromCamera();
                      Navigator.of(context).pop();
                    },
                  ),
                ],
              ),
            ),
          );
        });
  }

  void _showPickerUser(context) {
    showModalBottomSheet(
        context: context,
        builder: (BuildContext bc) {
          return SafeArea(
            child: Container(
              child: new Wrap(
                children: <Widget>[
                  new ListTile(
                      leading: new Icon(Icons.photo_library),
                      title: new Text('Photo Library'),
                      onTap: () {
                        _userFromGallery();
                        Navigator.of(context).pop();
                      }),
                  new ListTile(
                    leading: new Icon(Icons.photo_camera),
                    title: new Text('Camera'),
                    onTap: () {
                      _userFromCamera();
                      Navigator.of(context).pop();
                    },
                  ),
                ],
              ),
            ),
          );
        });
  }

  File imageFile;
  File ktpImage;

  //NOTE:Pick image from camera
  _ktpFromCamera() async {
    File image = await ImagePicker.pickImage(
        source: ImageSource.camera, imageQuality: 50);

    setState(() {
      ktpImage = image;
    });
  }

  //NOTE:Pick image from gallery
  _ktpFromGallery() async {
    File image = await ImagePicker.pickImage(
        source: ImageSource.gallery, imageQuality: 50);

    setState(() {
      ktpImage = image;
    });
  }

  //NOTE:Pick image from camera
  _userFromCamera() async {
    File image = await ImagePicker.pickImage(
        source: ImageSource.camera, imageQuality: 50);

    setState(() {
      imageFile = image;
    });
  }

  //NOTE:Pick image from gallery
  _userFromGallery() async {
    File image = await ImagePicker.pickImage(
        source: ImageSource.gallery, imageQuality: 50);

    setState(() {
      imageFile = image;
    });
  }

  _registerUser() async {
    var request = new http.MultipartRequest(
        "POST", Uri.parse("http://duoshinefitness.net/api/dsf/member"));
    request.fields["member_name"] = _usernameController.text;
    var photo =
        await http.MultipartFile.fromPath("member_photo", imageFile.path);
    var ktp_photo =
        await http.MultipartFile.fromPath("ktp_photo", ktpImage.path);
    request.fields["member_dob"] = _dateOfBirthController.text;
    request.fields["member_phone"] = _phoneController.text;
    request.fields["member_add"] = _addressController.text;
    request.fields["member_email"] = _emailController.text;
    request.fields["branch_id"] = branch_id;
    request.fields["password"] = _passwordController.text;
    request.fields["member_idcard"] = _idCardController.text;
    request.fields["member_gender"] = "male";
    request.fields["src_branch"] = "s";
    request.files.add(photo);
    request.files.add(ktp_photo);
    var response = await request.send();

    response.stream.transform(utf8.decoder).listen((value) {
      print(value);
    });
  }

  Future<String> _memberBranch() async {
    await http
        .get('http://duoshinefitness.net/api/dsf/branch/member-branch')
        .then((response) {
      var data = json.decode(response.body);
      if(data["success"] == true){


      }else if(data["success"] == false){
        setState(() {
          _isLoading = false;
        });
      }
      setState(() {
        memberBranch = data['result'];
      });
    });
  }
}
