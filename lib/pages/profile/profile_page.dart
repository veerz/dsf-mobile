import 'package:dsf/pages/profile/change_password.dart';
import 'package:flutter/material.dart';
import 'package:flutter_icons/flutter_icons.dart';
import 'package:http/http.dart' as http;
import 'dart:convert';

class ProfilePage extends StatefulWidget {
  final String username;
  final String branch_name;
  final String member_photo;
  final String ktp_photo;
  final String gender;
  final String dateOfBirth;
  final String email;
  final String address;
  final String phone_number;
  final int id_user;
  final String token;
  final int is_verified;

  ProfilePage(
      {this.dateOfBirth,
      this.username,
      this.branch_name,
      this.member_photo,
      this.ktp_photo,
      this.gender,
      this.email,
      this.address,
      this.phone_number,
      this.id_user,
      this.token,
      this.is_verified
      });

  @override
  _ProfilePageState createState() => _ProfilePageState();
}

class _ProfilePageState extends State<ProfilePage> {
  @override
  void initState() {
    print(widget.is_verified);
    super.initState();
  }
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.black,
      appBar: AppBar(
        iconTheme: IconThemeData(
          color: Colors.black, //change your color here
        ),
        backgroundColor: Color(0xFFf5c518),
        title: Text(
          "Profile",
          style: TextStyle(color: Color(0xFF050914)),
        ),
      ),
      body: Container(
        child: Column(
          children: [
            Card(
              child: ListTile(
                title: (widget.is_verified == 0) ? Row(
                  children: [
                    Text("Your email hasnt verified yet."),

                  ],
                ) : 
                Text("Your email was verified")
                ,
                subtitle: (widget.is_verified == 0) ?
                GestureDetector(
                  onTap: (){
                    resendEmailVerification();
                  },
                  child: Text("Verified now",
                    style: TextStyle(
                        color: Colors.blueAccent,
                        fontSize: 20
                    ),
                  ),
                ) : SizedBox(),
              ),
            ),
            
            Card(
              child: ListTile(
                leading: GestureDetector(
                  onTap: () {
                    showDialog(
                        context: context,
                        builder: (builder) {
                          return GestureDetector(
                            onTap: () {
                              Navigator.pop(context);
                            },
                            child: Container(
                              height: 350.0,
                              color: Colors
                                  .transparent, //could change this to Color(0xFF737373),
                              //so you don't have to change MaterialApp canvasColor
                              child: Image.network(widget.member_photo),
                            ),
                          );
                        });
                  },
                  child: CircleAvatar(
                    backgroundImage: NetworkImage(widget.member_photo),
                  ),
                ),
                title: Text(widget.username),
                subtitle: Text("${widget.address}"),
              ),
            ),
            Card(
              child: ListTile(
                leading: Icon(Ionicons.ios_calendar),
                title: Text("Date Of Birth"),
                subtitle: Text(widget.dateOfBirth.toString()),
              ),
            ),
            Card(
              child: ListTile(
                leading: Icon(Ionicons.ios_transgender),
                title: Text("Gender"),
                subtitle: Text("${widget.gender}"),
              ),
            ),
            Card(
              child: ListTile(
                leading: Icon(Ionicons.md_mail),
                title: Text("Email"),
                subtitle: Text("${widget.email}"),
              ),
            ),
            Card(
              child: ListTile(
                leading: Icon(Ionicons.ios_phone_portrait),
                title: Text("Phone Number"),
                subtitle: Text("${widget.phone_number}"),
              ),
            ),
            Card(
              child: ListTile(
                leading: Icon(Ionicons.ios_git_branch),
                title: Text("Branch"),
                subtitle: Text("${widget.branch_name}"),
              ),
            ),
            GestureDetector(
              onTap: (){
                  Navigator.push(context, MaterialPageRoute(
                    builder: (context) => ChangePasswordPage(
                      widget.id_user,
                      widget.token
                    )
                  ));
              },
              child: Card(
                child: ListTile(
                  leading: Icon(Ionicons.md_key,
                    color: Colors.blueAccent,
                  ),
                  title: Text("Edit Password",
                    style: TextStyle(
                      color: Colors.blueAccent
                    ),
                  ),
                ),
              ),
            ),
          ],
        ),
      ),
    );
  }

  void resendEmailVerification()async{
    final response = await http.get("http://duoshinefitness.net/api/dsf/auth/resend-email-verification/${widget.id_user}",
      headers: {
        "Authorization": widget.token,
      }
    );

    var result = json.decode(response.body);
    print(result);
  }
}
