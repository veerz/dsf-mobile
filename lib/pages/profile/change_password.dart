import 'package:flutter/material.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:http/http.dart' as http;
import 'dart:convert';

class ChangePasswordPage extends StatefulWidget {
  int id_user;
  String token;
  ChangePasswordPage(this.id_user,this.token);
  @override
  _ChangePasswordPageState createState() => _ChangePasswordPageState();
}

class _ChangePasswordPageState extends State<ChangePasswordPage> {
  bool _isObscure = true;
  bool _isObscure2 = true;
  bool _isObscure3 = true;
  TextEditingController _oldPasswordController = TextEditingController();
  TextEditingController _passwordController = TextEditingController();
  TextEditingController _confirmationPasswordController = TextEditingController();

  @override
  void initState() {
    print(widget.id_user);
    super.initState();
  }
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.black,
      appBar: AppBar(
        iconTheme: IconThemeData(
          color: Colors.black, //change your color here
        ),
        backgroundColor: Color(0xFFf5c518),
        title: Text(
          "Change Password",
          style: TextStyle(color: Color(0xFF050914)),
        ),
      ),
      body: Container(
        margin: EdgeInsets.only(left: 24,right: 24),
        child: Form(
          child: Column(
            mainAxisAlignment: MainAxisAlignment.center,
            children: [
              Card(
                elevation: 0.0,
                color: Colors.blueGrey[50],
                child: TextField(
                  controller: _oldPasswordController,
                  decoration: InputDecoration(
                    border: InputBorder.none,
                    labelText: 'Old Password',
                    prefixIcon: Icon(Icons.lock_outline),
                    suffixIcon: IconButton(
                      onPressed: () {
                        setState(() {
                          _isObscure = !_isObscure;
                        });
                      },
                      icon: Icon(_isObscure
                          ? Icons.visibility_off
                          : Icons.visibility),
                      color: Colors.black45,
                    ),
                  ),
                  obscureText: _isObscure,
                ),
              ),

              Card(
                elevation: 0.0,
                color: Colors.blueGrey[50],
                child: TextField(
                  controller: _passwordController,
                  decoration: InputDecoration(
                    border: InputBorder.none,
                    labelText: 'New Password',
                    prefixIcon: Icon(Icons.lock_outline),
                    suffixIcon: IconButton(
                      onPressed: () {
                        setState(() {
                          _isObscure2 = !_isObscure2;
                        });
                      },
                      icon: Icon(_isObscure2
                          ? Icons.visibility_off
                          : Icons.visibility),
                      color: Colors.black45,
                    ),
                  ),
                  obscureText: _isObscure2,
                ),
              ),

              Card(
                elevation: 0.0,
                color: Colors.blueGrey[50],
                child: TextField(
                  controller: _confirmationPasswordController,
                  decoration: InputDecoration(
                    border: InputBorder.none,
                    labelText: 'Confirm New Password',
                    prefixIcon: Icon(Icons.lock_outline),
                    suffixIcon: IconButton(
                      onPressed: () {
                        setState(() {
                          _isObscure3 = !_isObscure3;
                        });
                      },
                      icon: Icon(_isObscure3
                          ? Icons.visibility_off
                          : Icons.visibility),
                      color: Colors.black45,
                    ),
                  ),
                  obscureText: _isObscure3,
                ),
              ),

              MaterialButton(
                  minWidth: 277,
                  padding: EdgeInsets.all(10),
                  onPressed: (){
                    changePassword();
                  },
                  child: Text("SUBMIT",
                    style: GoogleFonts.getFont("Oswald",
                      fontSize: 15,
                      color: Colors.white,
                      fontWeight: FontWeight.w600
                    ),
                  ),
                color: Color(0xFFf5c518),
              ),

            ],
          ),
        ),
      ),
    );
  }

  void changePassword() async {
    final response = await http.post(
      "http://duoshinefitness.net/api/dsf/auth/change-password/${widget.id_user}",
      headers: {
        "Authorization" : widget.token,
      },
      body: {
        "old_password" : _oldPasswordController.text,
        "password" : _passwordController.text,
        "password_confirmation" : _confirmationPasswordController.text,
      },
    );

    var result = json.decode(response.body);

    print(result);

  }
}
