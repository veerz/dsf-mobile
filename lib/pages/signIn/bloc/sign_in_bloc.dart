import 'dart:async';
import 'package:bloc/bloc.dart';
import 'package:dsf/bloc/authentication_bloc.dart';
import 'package:dsf/bloc/authentication_event.dart';
import 'package:dsf/network_utils/repository/user_repository.dart';
import 'package:meta/meta.dart';
import './bloc.dart';

class SignInBloc extends Bloc<SignInEvent, SignInState> {
  final UserRepository userRepository;
  final AuthenticationBloc authenticationBloc;

  SignInBloc({
    @required this.userRepository,
    @required this.authenticationBloc,
  }) : assert(authenticationBloc != null);

  @override
  SignInState get initialState => SignInInitial();

  @override
  Stream<SignInState> mapEventToState(
    SignInEvent event,
  ) async* {
    if (event is SignInButtonPressed) {
      yield SignInLoading();

      try {
        final token = await userRepository.authenticate(
          username: event.username,
          password: event.password,
        );

        authenticationBloc.add(LoggedIn(token: token));
        yield SignInInitial();
      } catch (error) {
        yield SignInFailure(error: error.toString());
      }
    }
  }
}
