import 'package:flutter/material.dart';
import 'package:http/http.dart' as http;
import 'dart:convert';

class ForgotPasswordPage extends StatefulWidget {
  @override
  _ForgotPasswordPageState createState() => _ForgotPasswordPageState();
}

class _ForgotPasswordPageState extends State<ForgotPasswordPage> {
  TextEditingController _emailController = TextEditingController();
  String message = "";
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.black,
      appBar: AppBar(
        iconTheme: IconThemeData(
          color: Colors.black, //change your color here
        ),
        backgroundColor: Color(0xFFf5c518),
        title: Text(
          "Forgot Password",
          style: TextStyle(color: Color(0xFF050914)),
        ),
      ),
      body: Container(
        margin: EdgeInsets.all(15),
          child: Column(
            mainAxisAlignment: MainAxisAlignment.center,
            children: [
              Card(
                elevation: 0.0,
                color: Colors.blueGrey[50],
                child: TextField(
                  controller: _emailController,
                  decoration: InputDecoration(
                    border: InputBorder.none,
                    labelText: 'Phone/Email',
                    prefixIcon: Icon(Icons.person),
                  ),
                  keyboardType: TextInputType.emailAddress,
                ),
              ),
              
              MaterialButton(
                  minWidth: 277,
                  color: Colors.blueAccent,
                  child: Text("submit"),
                  onPressed: (){
                    forgotPassword();
                }
              ),

              Text(
                message,
                style: TextStyle(
                  color: Colors.green,
                  fontSize: 20
                ),
              ),
            ],
          ),
      ),
    );
  }

  void forgotPassword() async {
    final response = await http.post(
      "http://duoshinefitness.net/api/dsf/auth/forgot-password",
      body:{
        "email": _emailController.text
      }
    );
    
    var result = json.decode(response.body);
    print(result);

    if(result['success'] == true){
      setState(() {
        _emailController.text = "";
      });
    }

    setState(() {
      message == result;
    });

  }
}
