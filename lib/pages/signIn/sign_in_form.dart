import 'package:dsf/pages/Sign%20Up/sign_up_form.dart';
import 'package:dsf/pages/Sign%20Up/sign_up_page.dart';
import 'package:dsf/pages/signIn/forgot_password_page.dart';
import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:google_fonts/google_fonts.dart';
import 'dart:async';
import 'dart:convert';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:dsf/network_utils/repository/user_repository.dart';
import 'package:dsf/pages/home/home_page.dart';
import 'package:dsf/pages/signIn/bloc/bloc.dart';
import 'package:http/http.dart' as http;

class SignInForm extends StatefulWidget {
  @override
  _SignInFormState createState() => _SignInFormState();
}

class _SignInFormState extends State<SignInForm> {
  final GlobalKey<ScaffoldState> _scaffoldKey = new GlobalKey<ScaffoldState>();
  final _usernameController = TextEditingController();
  final _passwordController = TextEditingController();

  bool _isObscure = true;
  bool _isLoading = false;
  String _message = "";

  void _signInButtonPressed() {
    BlocProvider.of<SignInBloc>(context).add(
      SignInButtonPressed(
        username: _usernameController.text,
        password: _passwordController.text,
      ),
    );
  }

  @override
  Widget build(BuildContext context) {
    return BlocListener<SignInBloc, SignInState>(
      listener: (context, state) {
        if (state is SignInFailure) {
          Scaffold.of(context).showSnackBar(
            SnackBar(
              content: Text(state.error.toString()),
              backgroundColor: Colors.red,
            ),
          );
        }
      },
      child: BlocBuilder<SignInBloc, SignInState>(
        builder: (context, state) {
          return SafeArea(
            child: Center(
              child: SingleChildScrollView(
                padding: EdgeInsets.symmetric(horizontal: 32.0, vertical: 16.0),
                physics: BouncingScrollPhysics(),
                child: Column(
                  mainAxisAlignment: MainAxisAlignment.center,
                  mainAxisSize: MainAxisSize.max,
                  children: <Widget>[
                    SizedBox(height: 8.0),
                    Row(
                      crossAxisAlignment: CrossAxisAlignment.center,
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: <Widget>[
                        Text(
                          'Duo',
                          style: GoogleFonts.rubik(
                            fontWeight: FontWeight.w800,
                            fontSize: 32.0,
                            color: Colors.white,
                          ),
                        ),
                        SizedBox(
                          width: 5,
                        ),
                        Text(
                          'Shine Fitness',
                          style: GoogleFonts.rubik(
                              fontWeight: FontWeight.w400,
                              fontSize: 32.0,
                              color: Colors.yellow[600]),
                        ),
                      ],
                    ),
                    SizedBox(height: 10),
                    Text(
                      'Welome back! Sign in to your account.',
                      style: TextStyle(
                          color: Colors.white, fontWeight: FontWeight.w600),
                    ),
                    SizedBox(height: 56.0),
                    Text(_message,
                      style: TextStyle(
                        fontSize: 20,
                        color: Colors.redAccent
                      ),
                    ),
                    Card(
                      elevation: 0.0,
                      color: Colors.blueGrey[50],
                      child: TextField(
                        controller: _usernameController,
                        decoration: InputDecoration(
                          border: InputBorder.none,
                          labelText: 'Phone/Email',
                          prefixIcon: Icon(Icons.person),
                        ),
                        keyboardType: TextInputType.emailAddress,
                      ),
                    ),
                    SizedBox(height: 16.0),
                    Card(
                      elevation: 0.0,
                      color: Colors.blueGrey[50],
                      child: TextField(
                        controller: _passwordController,
                        decoration: InputDecoration(
                          border: InputBorder.none,
                          labelText: 'Password',
                          prefixIcon: Icon(Icons.lock_outline),
                          suffixIcon: IconButton(
                            onPressed: () {
                              setState(() {
                                _isObscure = !_isObscure;
                              });
                            },
                            icon: Icon(_isObscure
                                ? Icons.visibility_off
                                : Icons.visibility),
                            color: Colors.black45,
                          ),
                        ),
                        obscureText: _isObscure,
                      ),
                    ),
                    SizedBox(height: 32.0),

                    SizedBox(
                      height: 10,
                    ),
                    Container(
                      width: MediaQuery.of(context).size.width,
                      child: RaisedButton(
                        color: Color(0xFFf5c518),
                        child: (_isLoading == false)
                            ? Text(
                                "Sign In",
                                style: TextStyle(color: Colors.white),
                              )
                            : CircularProgressIndicator(
                                backgroundColor: Colors.blueAccent,
                              ),
                        onPressed: () {
                          setState(() {
                            _isLoading = true;
                          });
                          signin();
                        },
                      ),
                    ),
                    Container(
                      width: MediaQuery.of(context).size.width,
                      margin: EdgeInsets.fromLTRB(0, 10, 0, 0),
                      child: Center(
                        child: GestureDetector(
                          child: Text(
                                  "Sign Up",
                                  style: TextStyle(color: Colors.white),
                                ),
                          onTap: () {
                            Navigator.push(
                                context,
                                MaterialPageRoute(
                                    builder: (context) => SignUpPage()));
                          },
                        ),
                      ),
                    ),
                    Container(
                      width: MediaQuery.of(context).size.width,
                      margin: EdgeInsets.fromLTRB(0, 10, 0, 0),
                      child: Center(
                        child: GestureDetector(
                          child: Text(
                            "Forgot Password?",
                            style: TextStyle(color: Colors.grey,
                              fontStyle: FontStyle.italic
                            ),
                          ),
                          onTap: () {
                            Navigator.push(
                                context,
                                MaterialPageRoute(
                                    builder: (context) => ForgotPasswordPage()));
                          },
                        ),
                      ),
                    ),
                  ],
                ),
              ),
            ),
          );
        },
      ),
    );
  }

  Future<void> signin() async {
    final response = await http.post(
      //'http://192.168.1.100/sales-track/api/signin',
      'http://duoshinefitness.net/api/dsf/auth/signin',
      headers: {
        "Charset": "utf-8",
        "Content-Type": "application/json",
      },
      body: json.encode({
        'phone': _usernameController.text,
        'password': _passwordController.text,
      }),
    );
    var result = json.decode(response.body);
    print(result);
    if (result['success'] == true) {
      SharedPreferences localStorage = await SharedPreferences.getInstance();
      localStorage.setString('token', result['token']);
      localStorage.setString('user', json.encode(result['result']));
      print(result['token']);
      Navigator.push(
        context,
        MaterialPageRoute(builder: (context) => HomePage()),
      );
    }else if(result['success'] == false){
      print(result);
      setState(() {
        _message = result['message'];
        _isLoading = false;
      });
    }
  }
}
