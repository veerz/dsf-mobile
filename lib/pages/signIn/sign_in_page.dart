import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:dsf/bloc/authentication_bloc.dart';
import 'package:dsf/network_utils/repository/user_repository.dart';
import 'package:dsf/pages/signIn/bloc/sign_in_bloc.dart';
import 'package:dsf/pages/signIn/sign_in_form.dart';

class SignInPage extends StatelessWidget {
  final GlobalKey<ScaffoldState> _scaffoldKey = new GlobalKey<ScaffoldState>();

  final UserRepository userRepository;

  SignInPage({Key key, @required this.userRepository}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.black,
      key: _scaffoldKey,
      body: BlocProvider(
        create: (context) {
          return SignInBloc(
            authenticationBloc: BlocProvider.of<AuthenticationBloc>(context),
          );
        },
        child: Stack(
          children: <Widget>[
//            FlareActor(
//              'assets/login.flr',
//              fit: BoxFit.cover,
//              alignment: FractionalOffset.topCenter,
//              animation: 'Flow',
//            ),
            SignInForm(),
          ],
        ),
      ),
    );
  }
}
