import 'package:dsf/pages/QrCode/qr_code.dart';
import 'package:dsf/pages/contract/contract_page.dart';
import 'package:dsf/pages/profile/profile_page.dart';
import 'package:dsf/pages/signIn/sign_in_page.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:page_transition/page_transition.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'dart:convert';
import 'package:intl/intl.dart';

import 'package:flutter_icons/flutter_icons.dart';
import 'package:http/http.dart' as http;

import '../../model/contract_model.dart';
import '../../services/contract_services.dart';

class HomePage extends StatefulWidget {
  @override
  _HomePageState createState() => _HomePageState();
}

class _HomePageState extends State<HomePage> {
  var colors = 0xFFF8EBE7;
  var fontcolor = 0xFF0B0965;
  List<Contract> _contractList;
  String name;
  int user_id;
  String cabang;
  String token;
  String member_photo;
  String ktp_photo;
  String gender;
  String dob;
  String email;
  String address;
  String phone;
  int _isVerified;

  @override
  void initState() {
    _loadUserData();

    super.initState();
  }

  _loadUserData() async {


    SharedPreferences localStorage = await SharedPreferences.getInstance();
    var user = json.decode(localStorage.getString('user'));
    var tkn = localStorage.getString('token');

    ContractServices.getMyContract(user['member_id'], tkn)
        .then((contractServices) {
      setState(() {
        _contractList = contractServices;
      });
    });

    if (user != null) {
      setState(() {
        phone = user['member_phone'];
        address = user['member_add'];
        email = user['member_email'];
        dob = user['member_dob'];
        gender = user['member_gender'];
        user_id = user['member_id'];
        name = user['member_name'];
        cabang = user['branch_name'];
        token = tkn;
        member_photo = user['member_photo'];
        ktp_photo = user['ktp_photo'];
        _isVerified = user['verified_email'];
      });
    }
    print(user);
    print(user_id);
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.black,
      body: Stack(
        children: <Widget>[
          Container(
            height: MediaQuery.of(context).size.height / 5,
            width: MediaQuery.of(context).size.width,
            decoration: BoxDecoration(color: Color(0xFFf5c518)),
          ),
          Column(
            children: <Widget>[
              SizedBox(
                height: 70,
              ),
              Padding(
                padding: EdgeInsets.only(left: 16, right: 16),
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: <Widget>[
                    Row(
                      children: <Widget>[
                        Container(
                          decoration: BoxDecoration(
                            color: Colors.white,
                            borderRadius: BorderRadius.all(Radius.circular(50)),
                            boxShadow: [
                              BoxShadow(
                                color: Colors.black.withOpacity(0.2),
                                offset: Offset(0, 7),
                                blurRadius: 20,
                                spreadRadius: 1,
                              )
                            ],
                          ),
                          width: 50,
                          child: Container(),
                        ),
                        SizedBox(
                          width: 15,
                        ),
                        Container(
                          padding: EdgeInsets.only(bottom: 20),
                          child: Column(
                            children: <Widget>[
                              Text(
                                "$name",
                                style: GoogleFonts.openSans(
                                  textStyle: TextStyle(
                                      color: Colors.black,
                                      fontSize: 22,
                                      fontWeight: FontWeight.w800),
                                ),
                              ),
                              Text(
                                "$cabang",
                                style: GoogleFonts.openSans(
                                  textStyle: TextStyle(
                                      color: Colors.black,
                                      fontSize: 16,
                                      fontWeight: FontWeight.w600),
                                ),
                              ),
                            ],
                          ),
                        ),
                      ],
                    ),
                    IconButton(
                        onPressed: () {
                          CupertinoAlertDialog alertDialog =
                              CupertinoAlertDialog(
                            title: Text('Confirm Log Out'),
                            content: Text('Are you sure you want to log out?'),
                            actions: <Widget>[
                              CupertinoDialogAction(
                                onPressed: () {
                                  Navigator.pop(context);
                                },
                                child: Text('Cancel'),
                              ),
                              CupertinoDialogAction(
                                onPressed: () {
                                  Navigator.pop(context);
                                  _signout();
                                },
                                isDestructiveAction: true,
                                child: Text('Yes'),
                              ),
                            ],
                          );

                          showDialog(
                              context: context,
                              builder: (context) => alertDialog);
                        },
                        alignment: Alignment.topCenter,
                        icon: Icon(
                          AntDesign.logout,
                          color: Colors.red,
                        )),
                  ],
                ),
              ),

              Container(
                height: MediaQuery.of(context).size.height/2,
                color: Colors.black,
                child: (_contractList != null) ? ListView.builder(
                    itemCount: _contractList.length,
                    itemBuilder: (context,index){
                      String _dueDate = DateFormat("yyyy-MM-dd")
                          .format(_contractList[index].dueDate);
                      String _fromDate = DateFormat("yyyy-MM-dd")
                          .format(_contractList[index].fromDate);
                      return Card(
                        color: Colors.black,
                        shape: RoundedRectangleBorder(
                            side: BorderSide(width: 5, color: Colors.white)),
                        child: ListTile(
                          title: Row(
                            mainAxisAlignment: MainAxisAlignment.spaceBetween,
                            children: [
                              Padding(
                                padding: const EdgeInsets.all(8.0),
                                child: Text("\#" +
                                    _contractList[index].kontrakId.toString(),
                                  style: TextStyle(
                                    color: Colors.white
                                  ),
                                ),
                              ),
                            ],
                          ),
                          subtitle: Column(
                            children: [
                              Row(
                                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                                children: [
                                  Container(
                                    margin: EdgeInsets.only(top: 17),
                                    child: Row(
                                      mainAxisAlignment: MainAxisAlignment.center,
                                      children: [
                                        Text(
                                          _contractList[index].paket.paketName,
                                          style: TextStyle(
                                              color: Colors.white,
                                              fontWeight: FontWeight.bold,
                                              fontSize: 22),
                                        ),
                                      ],
                                    ),
                                  ),
                                ],
                              ),
                              Container(
                                margin: EdgeInsets.only(top: 10),
                                child: Row(
                                  children: [
                                    SizedBox(width: 100, child: Text("Cabang",
                                      style: TextStyle(
                                          color: Colors.white
                                      ),
                                    )),
                                    SizedBox(
                                        width: 200,
                                        child: Text(
                                          cabang,
                                          style: TextStyle(
                                              color: Colors.white,
                                              fontSize: 20),
                                        )),
                                  ],
                                ),
                              ),
                              Container(
                                margin: EdgeInsets.only(top: 10),
                                child: Row(
                                  children: [
                                    SizedBox(width: 100, child: Text("Durasi",
                                      style: TextStyle(
                                        color: Colors.white
                                      ),
                                    )),
                                    SizedBox(
                                        width: 200,
                                        child: Text(
                                          _contractList[index]
                                              .paket
                                              .duration
                                              .toString() +
                                              " Month",
                                          style: TextStyle(
                                              color: Colors.white,
                                              fontSize: 20),
                                        )),
                                  ],
                                ),
                              ),
                              Container(
                                margin: EdgeInsets.only(top: 10),
                                child: Row(
                                  children: <Widget>[
                                    SizedBox(width: 100, child: Text("Harga ",
                                      style: TextStyle(
                                        color: Colors.white
                                      ),
                                    )),
                                    SizedBox(
                                        width: 200,
                                        child: Text(
                                          "Rp." +
                                              _contractList[index]
                                                  .paket
                                                  .paketValue
                                                  .toString(),
                                          style: TextStyle(
                                              color: Colors.white,
                                              fontSize: 20),
                                        )),
                                  ],
                                ),
                              ),
                              Container(
                                margin: EdgeInsets.only(top: 10),
                                child: Row(
                                  children: [
                                    SizedBox(width: 100, child: Text("From Date",
                                      style: TextStyle(
                                        color: Colors.white
                                      ),
                                    )),
                                    SizedBox(
                                        width: 200,
                                        child: Text(
                                          _dueDate,
                                          style: TextStyle(
                                              color: Colors.white,
                                              fontSize: 20),
                                        )),
                                  ],
                                ),
                              ),
                              Container(
                                margin: EdgeInsets.only(top: 10),
                                child: Row(
                                  children: [
                                    SizedBox(width: 100, child: Text("Due Date",
                                      style: TextStyle(
                                        color: Colors.white
                                      ),
                                    )),
                                    SizedBox(
                                        width: 200,
                                        child: Text(
                                          _fromDate,
                                          style: TextStyle(
                                              color: Colors.white,
                                              fontSize: 20),
                                        )),
                                  ],
                                ),
                              ),
                              Container(
                                margin: EdgeInsets.only(top: 10),
                                child: Row(
                                  children: [
                                    SizedBox(width: 100, child: Text("Prioritas",
                                      style: TextStyle(
                                        color: Colors.white
                                      ),
                                    )),
                                    SizedBox(
                                        width: 100,
                                        child: Text(
                                          (_contractList[index].isPrioritas == 1)
                                              ? "Yes"
                                              : "Non-No",
                                          style: GoogleFonts.getFont("Oswald",
                                              fontSize: 20,
                                              fontWeight: FontWeight.bold,
                                              color: Colors.greenAccent),
                                        )),
                                  ],
                                ),
                              ),
                              Container(
                                  margin: EdgeInsets.only(top: 10, bottom: 10),
                                  child: Row(
                                    crossAxisAlignment: CrossAxisAlignment.start,
                                    children: [
                                      SizedBox(
                                          width: 100, child: Text("KETERANGAN")),
                                      SizedBox(
                                          width: 200,
                                          child: Expanded(
                                              child: Text(_contractList[index]
                                                  .kontrakKet))),
                                    ],
                                  )),
                            ],
                          ),
                        ),
                      );
                    }) : SizedBox(),
              ),

              Container(
                margin: EdgeInsets.only(top: MediaQuery.of(context).size.height/10),
                width: MediaQuery.of(context).size.width - 40,
                height: MediaQuery.of(context).size.height / 6,
                decoration: BoxDecoration(
                  color: Color(0xFFf5c518),
                  borderRadius: BorderRadius.all(Radius.circular(20)),
                  boxShadow: [
                    BoxShadow(
                      color: Color(0xFFf5c518).withOpacity(0.2),
                      offset: Offset(0, 12),
                      blurRadius: 30,
                      spreadRadius: 0,
                    )
                  ],
                ),
                child: Container(
                  margin: EdgeInsets.only(left: 40),
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.spaceAround,
                    children: <Widget>[
                      //NOTE:User button
                      Container(
                        child: Column(
                          mainAxisAlignment: MainAxisAlignment.center,
                          crossAxisAlignment: CrossAxisAlignment.center,
                          children: <Widget>[
                            Row(
                              children: <Widget>[
                                GestureDetector(
                                  onTap: () {
                                    Navigator.push(
                                      context,
                                      MaterialPageRoute(
                                        builder: (context) => ProfilePage(
                                          phone_number: phone,
                                          address: address,
                                          email: email,
                                          dateOfBirth: dob,
                                          username: name,
                                          branch_name: cabang,
                                          member_photo: member_photo,
                                          ktp_photo: ktp_photo,
                                          gender: gender,
                                          id_user: user_id,
                                          token: token,
                                          is_verified: _isVerified,
                                        ),
                                      ),
                                    );
                                  },
                                  child: Container(
                                    margin: EdgeInsets.only(top: 2),
                                    child: Column(
                                      crossAxisAlignment:
                                          CrossAxisAlignment.center,
                                      mainAxisAlignment:
                                          MainAxisAlignment.center,
                                      children: <Widget>[
                                        Icon(
                                          EvilIcons.user,
                                          color: Colors.black,
                                          size: 40,
                                        ),
                                        SizedBox(
                                          height: 5,
                                        ),
                                        Text(
                                          "Profile",
                                          style: GoogleFonts.openSans(
                                            textStyle: TextStyle(
                                              color: Colors.black,
                                              fontSize: 12,
                                              fontWeight: FontWeight.w400,
                                            ),
                                          ),
                                        ),
                                      ],
                                    ),
                                  ),
                                ),
                              ],
                            ),
                          ],
                        ),
                      ),
                      SizedBox(
                        width: 20,
                      ),
                      //NOTE:Product
                      GestureDetector(
                        onTap: () {
                          Navigator.push(
                            context,
                            MaterialPageRoute(
                                builder: (context) => MyContractPage(
                                      cabang: cabang,
                                      id_user: user_id,
                                      token: token,
                                    )),
                          );
                        },
                        child: Container(
                          margin: EdgeInsets.only(top: 7),
                          child: Column(
                            mainAxisAlignment: MainAxisAlignment.center,
                            crossAxisAlignment: CrossAxisAlignment.center,
                            children: <Widget>[
                              Icon(
                                  IconData(0xe06f,
                                      fontFamily: 'MaterialIcons',
                                      matchTextDirection: true),
                                  size: 35),
                              SizedBox(
                                height: 5,
                              ),
                              Text(
                                "My Contract",
                                style: GoogleFonts.openSans(
                                  textStyle: TextStyle(
                                    color: Colors.black,
                                    fontSize: 12,
                                    fontWeight: FontWeight.w400,
                                  ),
                                ),
                              ),
                            ],
                          ),
                        ),
                      ),
                      SizedBox(
                        width: 20,
                      ),
                      //NOTE:Supplier
                      GestureDetector(
                        onTap: () {
                          Navigator.push(
                            context,
                            MaterialPageRoute(
                                builder: (context) =>
                                    QrCodePage(user_id, name)),
                          );
                        },
                        child: Container(
                          margin: EdgeInsets.only(top: 5),
                          child: Column(
                            crossAxisAlignment: CrossAxisAlignment.center,
                            mainAxisAlignment: MainAxisAlignment.center,
                            children: <Widget>[
                              Icon(
                                Ionicons.md_qr_scanner,
                                color: Colors.black,
                                size: 35,
                              ),
                              SizedBox(
                                height: 5,
                              ),
                              Text(
                                "QR code",
                                style: GoogleFonts.openSans(
                                  textStyle: TextStyle(
                                    color: Colors.black,
                                    fontSize: 12,
                                    fontWeight: FontWeight.w400,
                                  ),
                                ),
                              ),
                            ],
                          ),
                        ),
                      ),
                      SizedBox(
                        width: 20,
                      ),
                    ],
                  ),
                ),
              ),
              SizedBox(
                height: 40,
              ),
            ],
          ),
        ],
      ),
    );
  }

  void _signout() async {
    SharedPreferences localStorage = await SharedPreferences.getInstance();
    localStorage.remove('user');
    localStorage.remove('token');
    Navigator.push(
      context,
      MaterialPageRoute(builder: (context) => SignInPage()),
    );
  }
}
