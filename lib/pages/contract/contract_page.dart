import 'package:dsf/model/contract_model.dart';
import 'package:dsf/services/contract_services.dart';
import 'package:flutter/material.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:intl/intl.dart';

class MyContractPage extends StatefulWidget {
  int id_user;
  String token;
  String cabang;
  MyContractPage({this.id_user, this.token,this.cabang});
  @override
  _MyContractPageState createState() => _MyContractPageState();
}

class _MyContractPageState extends State<MyContractPage> {
  List<Contract> _contractList;
  List<Paket> _paketList;
  @override
  void initState() {
    ContractServices.getMyContract(widget.id_user, widget.token)
        .then((contractServices) {
      setState(() {
        _contractList = contractServices;
      });
    });
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.black,
      appBar: AppBar(
        iconTheme: IconThemeData(
          color: Colors.black, //change your color here
        ),
        backgroundColor: Color(0xFFf5c518),
        title: Text(
          "MY CONTRACT",
          style: GoogleFonts.getFont("Oswald", color: Colors.black),
        ),
      ),
      body: Container(
        child: (_contractList != null)
            ? ListView.builder(
                itemCount: _contractList.length,
                itemBuilder: (context, index) {
                  String _dueDate = DateFormat("yyyy-MM-dd")
                      .format(_contractList[index].dueDate);
                  String _fromDate = DateFormat("yyyy-MM-dd")
                      .format(_contractList[index].fromDate);
                  return Card(
                    shape: RoundedRectangleBorder(
                        side: BorderSide(width: 5, color: Colors.black)),
                    child: ListTile(
                      title: Row(
                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                        children: [
                          Padding(
                            padding: const EdgeInsets.all(8.0),
                            child: Text("\#" +
                                _contractList[index].kontrakId.toString()),
                          ),
                        ],
                      ),
                      subtitle: Column(
                        children: [
                          Row(
                            mainAxisAlignment: MainAxisAlignment.spaceBetween,
                            children: [
                              Container(
                                margin: EdgeInsets.only(top: 17),
                                child: Row(
                                  mainAxisAlignment: MainAxisAlignment.center,
                                  children: [
                                    Text(
                                      _contractList[index].paket.paketName,
                                      style: TextStyle(
                                          fontWeight: FontWeight.bold,
                                          fontSize: 22),
                                    ),
                                  ],
                                ),
                              ),
                            ],
                          ),
                          Container(
                            margin: EdgeInsets.only(top: 10),
                            child: Row(
                              children: [
                                SizedBox(width: 100, child: Text("Cabang",
                                  style: TextStyle(
                                      color: Colors.black
                                  ),
                                )),
                                SizedBox(
                                    width: 200,
                                    child: Text(
                                      widget.cabang,
                                      style: TextStyle(
                                          color: Colors.black,
                                          fontSize: 20),
                                    )),
                              ],
                            ),
                          ),
                          Container(
                            margin: EdgeInsets.only(top: 10),
                            child: Row(
                              children: [
                                SizedBox(width: 100, child: Text("Durasi")),
                                SizedBox(
                                    width: 200,
                                    child: Text(
                                      _contractList[index]
                                              .paket
                                              .duration
                                              .toString() +
                                          " Month",
                                      style: TextStyle(fontSize: 20),
                                    )),
                              ],
                            ),
                          ),
                          Container(
                            margin: EdgeInsets.only(top: 10),
                            child: Row(
                              children: <Widget>[
                                SizedBox(width: 100, child: Text("Harga ")),
                                SizedBox(
                                    width: 200,
                                    child: Text(
                                      "Rp." +
                                          _contractList[index]
                                              .paket
                                              .paketValue
                                              .toString(),
                                      style: TextStyle(fontSize: 20),
                                    )),
                              ],
                            ),
                          ),
                          Container(
                            margin: EdgeInsets.only(top: 10),
                            child: Row(
                              children: [
                                SizedBox(width: 100, child: Text("From Date")),
                                SizedBox(
                                    width: 200,
                                    child: Text(
                                      _dueDate,
                                      style: TextStyle(fontSize: 20),
                                    )),
                              ],
                            ),
                          ),
                          Container(
                            margin: EdgeInsets.only(top: 10),
                            child: Row(
                              children: [
                                SizedBox(width: 100, child: Text("Due Date")),
                                SizedBox(
                                    width: 200,
                                    child: Text(
                                      _fromDate,
                                      style: TextStyle(fontSize: 20),
                                    )),
                              ],
                            ),
                          ),
                          Container(
                            margin: EdgeInsets.only(top: 10),
                            child: Row(
                              children: [
                                SizedBox(width: 100, child: Text("PRIORITAS")),
                                SizedBox(
                                    width: 100,
                                    child: Text(
                                      (_contractList[index].isPrioritas == 1)
                                          ? "Yes"
                                          : "Non-No",
                                      style: GoogleFonts.getFont("Oswald",
                                          fontSize: 20,
                                          fontWeight: FontWeight.bold,
                                          color: Colors.greenAccent),
                                    )),
                              ],
                            ),
                          ),
                          Container(
                              margin: EdgeInsets.only(top: 10, bottom: 10),
                              child: Row(
                                crossAxisAlignment: CrossAxisAlignment.start,
                                children: [
                                  SizedBox(
                                      width: 100, child: Text("KETERANGAN")),
                                  SizedBox(
                                      width: 200,
                                      child: Expanded(
                                          child: Text(_contractList[index]
                                              .kontrakKet))),
                                ],
                              )),
                        ],
                      ),
                    ),
                  );
                })
            : SizedBox(),
      ),
    );
  }
}
