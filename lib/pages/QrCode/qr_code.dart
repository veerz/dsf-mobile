import 'package:flutter/material.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:qr_flutter/qr_flutter.dart';

class QrCodePage extends StatefulWidget {
  int member_id;
  String member_name;
  QrCodePage(this.member_id,this.member_name);
  @override
  _QrCodePageState createState() => _QrCodePageState();
}

class _QrCodePageState extends State<QrCodePage> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.black,
      appBar: AppBar(
        iconTheme: IconThemeData(
          color: Colors.black, //change your color here
        ),
        backgroundColor: Color(0xFFf5c518),
        title: Text("QR CODE",
          style: GoogleFonts.getFont("Oswald",
              color: Colors.black
          ),
        ),
      ),
      body: Container(
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.center,
          children: [
            Container(
                margin: EdgeInsets.all(20),
                child: Text(widget.member_name,
                  style: GoogleFonts.getFont("Oswald",
                    fontSize: 20,
                    color: Colors.white
                  ),
                )),
            Center(
              child: QrImage(
                version: QrVersions.auto,
                backgroundColor: Colors.white,
                size: 400.0,
                data: widget.member_id.toString(),
              ),
            ),
          ],
        ),
      ),
    );
  }
}
