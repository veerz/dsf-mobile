part of 'services.dart';

class registerServices {
  static Future<void> _registerUser({
      member_name,
      member_photo,
      ktp_photo,
      member_dob,
      member_gender,
      member_phone,
      member_add,
      member_email,
      branch_id,
      password,
      member_idcard,
      member_barcode
  })async{
    final response = await http.post(
      "http://duoshinefitness.net/api/dsf/member",
      body: json.encode({
        "member_name":member_name,
        "member_photo":member_phone,
        "ktp_photo":ktp_photo,
        "member_dob":member_dob,
        "member_gender":member_gender,
        "member_phone":member_phone,
        "member_add":member_add,
        "member_email":member_email,
        "branch_id":branch_id,
        "password":password,
        "member_idcard":member_idcard,
        "member_barcode":member_barcode,
      }),
    );

    var result = json.decode(response.body);
    print(result);
  }

}