import 'dart:convert';

import 'package:http/http.dart' as http;
import 'package:dsf/model/contract_model.dart';

class ContractServices{
  static const String url = "http://duoshinefitness.net/api/dsf/contract-member/49?per_page=10&page=1";

  static Future<List<Contract>> getMyContract(id_user,token) async {
    try {
      final response = await http.get("http://duoshinefitness.net/api/dsf/contract-member/$id_user",
          headers: {
            "Authorization": token,
            "Content-Type": "application/json",
          }

      );
      var result = json.decode(response.body);
      print(result);
      if (response.statusCode == 200) {
        List<Contract> list = parseMyContract(response.body);
        return list;
      } else {
        throw Exception("Error");
      }
    } catch (e) {
      throw Exception(e.toString());
    }
  }

  static List<Contract> parseMyContract(String responseBody) {
    final parsed = json.decode(responseBody)['result'].cast<Map<String, dynamic>>();
    print(parsed);
    return parsed.map<Contract>((json) => Contract.fromJson(json)).toList();
  }
}