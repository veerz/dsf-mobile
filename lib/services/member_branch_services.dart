part of 'services.dart';

class MemberBranchServices{
  static const String url = "http://duoshinefitness.net/api/dsf/branch/member-branch";

  static Future<List<MemberBranch>> getMemberBranch() async {
    try {
      final response = await http.get(url,

      );
      if (response.statusCode == 200) {
        List<MemberBranch> list = parseMemberBranch(response.body);
        return list;
      } else {
        throw Exception("Error");
      }
    } catch (e) {
      throw Exception(e.toString());
    }
  }

  static List<MemberBranch> parseMemberBranch(String responseBody) {
    final parsed = json.decode(responseBody)['result'].cast<Map<String, dynamic>>();
    print(parsed);
    return parsed.map<MemberBranch>((json) => MemberBranch.fromJson(json)).toList();
  }
}