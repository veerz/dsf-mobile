class MemberBranch{
    final int branch_id;
    final String branch_name;
    final int id_owner;
    final int isPrioritas;
    final int target;
    final String branch_address;
    final String branch_city;
    final String branch_postcode;
    final String branch_phone;
    final String isDeleted;
    final String ts;

    MemberBranch({
      this.branch_id,
      this.branch_name,
      this.id_owner,
      this.isPrioritas,
      this.target,
      this.branch_address,
      this.branch_city,
      this.branch_postcode,
      this.branch_phone,
      this.isDeleted,
      this.ts,});

    factory MemberBranch.fromJson(Map<String,dynamic> parsedJson){
      return MemberBranch(
        branch_id: parsedJson['branch_id'],
        branch_name: parsedJson['branch_name'],
        id_owner: parsedJson['id_owner'],
        isPrioritas: parsedJson['isPrioritas'],
        target: parsedJson['target'],
        branch_address: parsedJson['branch_add'],
        branch_city: parsedJson['branch_city'],
        branch_postcode: parsedJson['branch_postcode'],
        branch_phone: parsedJson['branch_phone'],
        isDeleted: parsedJson['isDeleted'],
        ts: parsedJson['ts']
      );
    }
}