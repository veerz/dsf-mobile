
import 'dart:convert';

ContractResponse contractResponseFromJson(String str) => ContractResponse.fromJson(json.decode(str));

String contractResponseToJson(ContractResponse data) => json.encode(data.toJson());

class ContractResponse {
  ContractResponse({
    this.success,
    this.message,
    this.result,
  });

  bool success;
  String message;
  Result result;

  factory ContractResponse.fromJson(Map<String, dynamic> json) => ContractResponse(
    success: json["success"],
    message: json["message"],
    result: Result.fromJson(json["result"]),
  );

  Map<String, dynamic> toJson() => {
    "success": success,
    "message": message,
    "result": result.toJson(),
  };
}

class Result {
  Result({
    this.currentPage,
    this.data,
    this.firstPageUrl,
    this.from,
    this.lastPage,
    this.lastPageUrl,
    this.nextPageUrl,
    this.path,
    this.perPage,
    this.prevPageUrl,
    this.to,
    this.total,
  });

  int currentPage;
  List<Contract> data;
  String firstPageUrl;
  int from;
  int lastPage;
  String lastPageUrl;
  String nextPageUrl;
  String path;
  String perPage;
  dynamic prevPageUrl;
  int to;
  int total;

  factory Result.fromJson(Map<String, dynamic> json) => Result(
    currentPage: json["current_page"],
    data: List<Contract>.from(json["data"].map((x) => Contract.fromJson(x))),
    firstPageUrl: json["first_page_url"],
    from: json["from"],
    lastPage: json["last_page"],
    lastPageUrl: json["last_page_url"],
    nextPageUrl: json["next_page_url"],
    path: json["path"],
    perPage: json["per_page"],
    prevPageUrl: json["prev_page_url"],
    to: json["to"],
    total: json["total"],
  );

  Map<String, dynamic> toJson() => {
    "current_page": currentPage,
    "data": List<dynamic>.from(data.map((x) => x.toJson())),
    "first_page_url": firstPageUrl,
    "from": from,
    "last_page": lastPage,
    "last_page_url": lastPageUrl,
    "next_page_url": nextPageUrl,
    "path": path,
    "per_page": perPage,
    "prev_page_url": prevPageUrl,
    "to": to,
    "total": total,
  };
}

class Contract {
  Contract({
    this.kontrakId,
    this.memberId,
    this.paketId,
    this.fromDate,
    this.dueDate,
    this.branchId,
    this.empId,
    this.userId,
    this.srcBranch,
    this.kontrakKet,
    this.isApprove,
    this.updatestatusby,
    this.isPrioritas,
    this.ts,
    this.updatetime,
    this.paket,
  });

  int kontrakId;
  int memberId;
  int paketId;
  DateTime fromDate;
  DateTime dueDate;
  int branchId;
  int empId;
  int userId;
  int srcBranch;
  String kontrakKet;
  int isApprove;
  int updatestatusby;
  int isPrioritas;
  DateTime ts;
  Updatetime updatetime;
  Paket paket;

  factory Contract.fromJson(Map<String, dynamic> json) => Contract(
    kontrakId: json["kontrak_id"],
    memberId: json["member_id"],
    paketId: json["paket_id"],
    fromDate: DateTime.parse(json["from_date"]),
    dueDate: DateTime.parse(json["due_date"]),
    branchId: json["branch_id"],
    empId: json["emp_id"],
    userId: json["user_id"],
    srcBranch: json["src_branch"],
    kontrakKet: json["kontrak_ket"],
    isApprove: json["isApprove"],
    updatestatusby: json["updatestatusby"],
    isPrioritas: json["isPrioritas"],
    ts: DateTime.parse(json["ts"]),
    updatetime: updatetimeValues.map[json["updatetime"]],
    paket: Paket.fromJson(json["paket"]),
  );

  Map<String, dynamic> toJson() => {
    "kontrak_id": kontrakId,
    "member_id": memberId,
    "paket_id": paketId,
    "from_date": fromDate,
    "due_date": dueDate,
    "branch_id": branchId,
    "emp_id": empId,
    "user_id": userId,
    "src_branch": srcBranch,
    "kontrak_ket": kontrakKet,
    "isApprove": isApprove,
    "updatestatusby": updatestatusby,
    "isPrioritas": isPrioritas,
    "ts": ts.toIso8601String(),
    "updatetime": updatetimeValues.reverse[updatetime],
    "paket": paket.toJson(),
  };
}

class Paket {
  Paket({
    this.paketId,
    this.paketName,
    this.paketValue,
    this.duration,
  });

  int paketId;
  String paketName;
  int paketValue;
  int duration;

  factory Paket.fromJson(Map<String, dynamic> json) => Paket(
    paketId: json["paket_id"],
    paketName: json["paket_name"],
    paketValue: json["paket_value"],
    duration: json["duration"],
  );

  Map<String, dynamic> toJson() => {
    "paket_id": paketId,
    "paket_name": paketNameValues.reverse[paketName],
    "paket_value": paketValue,
    "duration": duration,
  };
}

enum PaketName { PERPANJANG_MEMBER_COWOK_1_BULAN }

final paketNameValues = EnumValues({
  "Perpanjang Member Cowok 1 Bulan": PaketName.PERPANJANG_MEMBER_COWOK_1_BULAN
});

enum Updatetime { THE_0000011130_T00_0000000000_Z }

final updatetimeValues = EnumValues({
  "-000001-11-30T00:00:00.000000Z": Updatetime.THE_0000011130_T00_0000000000_Z
});

class EnumValues<T> {
  Map<String, T> map;
  Map<T, String> reverseMap;

  EnumValues(this.map);

  Map<T, String> get reverse {
    if (reverseMap == null) {
      reverseMap = map.map((k, v) => new MapEntry(v, k));
    }
    return reverseMap;
  }
}
